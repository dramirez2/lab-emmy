//Propiedad intelectual de Julio de la Cruz, Ramon Collazo y Baniel Ramirezito
#include <iostream>
#include <cmath>
#include <math.h>
#include <fstream>
using namespace std;

typedef int* pi;
typedef pi* ppi;
typedef ppi* pppi;

//Factor Maximo Comun
void foo(int vars, int v, int mod, int *pol, int **powers, int ***sols, int alfa1, int alfa2, int alfa3, int &sumato){
      if(v == 0){ 
         // for(int m = 0; m < vars; m++)
         //   cout << pol[m] << " ";
         for(int m = 0; m < vars; m++){
           // cout << pol[m] << " ";
            // cout << powers[0][pol[m]] << "  "  << powers[1][pol[m]] << " " << powers[2][pol[m]] << endl;
            alfa1 += powers[0][pol[m]];
            alfa2 += powers[1][pol[m]];
            alfa3 += powers[2][pol[m]];
             // cout << "alfas: " << alfa1 << " " << alfa2 << " " << alfa3 << endl;
         }
         //cout << endl;
         alfa1 %= mod;
         alfa2 %= mod;
         alfa3 %= mod;
         // cout << "alfas: " << alfa1 << alfa2 << alfa3 << endl;
         sols[alfa1][alfa2][alfa3] = sols[alfa1][alfa2][alfa3] + 1;
         sumato += 1;
         alfa1 = alfa2 = alfa3 = 0;
         // cout << endl;
      return;
      }
      for(int i = 0; i < mod; i++){
         pol[v-1] = i;
        foo(vars, v-1, mod, pol, powers, sols, alfa1, alfa2, alfa3, sumato);
      }
      
}

int fmc(int a, int b){
   int temp; 
   while(1){
      temp = a % b;
      if(temp == 0)
         return b;
      a = b;
      b = temp;
   }
}

int powD(int base, int exp, int mod){
   int p = 1;
   int nbase = base;
   
   while(exp > 0){
      if (exp & 1){
         p = p * nbase;
         p = p % mod;
      }
      nbase = nbase %  mod;
      nbase = nbase * nbase;
      exp = exp >> 1;
   }  
   return p;
}

int main(){
   int *pol;
   int *powe;
   int *sol;
   int vars, exp_1, exp_2, exp_3, mod, alfa1, alfa2, alfa3;
   int *poly, *powers[3];
   pppi sols;
   ppi B;
   pi C, base;
   bool exps;
   double casos = 0;
   int sumato = 0;
   int v = 0;
   ofstream solus;
   solus.open("soluciones.txt");
   do{
      cout << "Exponente para la primera ecuacion: ";
      cin >> exp_1;
      while (exp_1 < 1){
         cout << "Exponente debe ser > 0." << endl;
         cout << "Exponente para la primera ecuacion: ";
         cin >> exp_1;
      }
      
      cout << "Exponente para la segunda ecuacion: ";
      cin >> exp_2;
      while (exp_2 < 1){
         cout << "Exponente debe ser > 0." << endl;
         cout << "Exponente para la segunda ecuacion: ";
         cin >> exp_2;
      }
   
      cout << "Exponente para la tercera ecuacion: ";
      cin >> exp_3;
      while (exp_3 < 1){
         cout << "Exponente debe ser > 0." << endl;
         cout << "Exponente para la tercera ecuacion: ";
         cin >> exp_3;
      }

      if (fmc(exp_1,exp_2) !=1 || fmc(exp_3,exp_2) !=1 || fmc(exp_1,exp_3) !=1){
         cout << "Exponentes deben ser relativamente primos." << endl;
         exps = false;
      }
      else exps = true;
   }while (!exps);

   vars = exp_1 + exp_2 + exp_3;
   poly = new int[vars];
      
   cout << "Modulo en el que se evaluara: ";
   cin >> mod;
   while (mod < 1){
      cout << "El modulo debe ser mayor que 1. Vuelva a introducirlo: ";
      cin >> mod;
   }

// Arreglo que guarda las variables a las potencias ya moduladas
   for(int i = 0; i < 3; i++){
      C = new int[mod];
      powers[i] = C; 
      for(int j = 0; j < mod; j++){
         if(i==0){
            powers[i][j] = powD(j, exp_1, mod);
            //cout << j << "^" << exp_1 << " " << powers[i][j] << " ||";
         }
         else if(i == 1){
            powers[i][j] = powD(j, exp_2, mod);
            // cout << j << "^" << exp_2 << " " << powers[i][j] << " ||";
         }
         else if(i==2){
            powers[i][j] = powD(j, exp_3, mod);
            // cout << j << "^" << exp_3 << " " << powers[i][j] << " ||";
         }
      }
      cout << endl;
   }

   for(int k = 0; k < 3; k++){
      for (int l = 0; l < mod; l++){
         cout << powers[k][l] << " ";
      }
      cout << endl;
   }
   sols = new ppi[mod];
   for(int i = 0; i < mod; i++){
      B = new pi[mod];
      for(int j = 0; j < mod; j++){
         C = new int[mod];
         B[j] = C;
      }
      sols[i] = B;
   }
 
   for (int i = 0; i < mod; i++)
      for (int j = 0; j < mod; j++)
         for (int k = 0; k < mod; k++)
            sols[i][j][k] = 0;
   
   base = new int[vars];
   for(int j = 0; j < vars; j++){
      base[j] = (int)(pow((double)mod, (double)j));
         // C = new int[vars];
         // base[j] = C;
         // for (int v = 0; v < vars; v++){
         //    base[j][v] = (int)(pow((double)mod, (double)v)) % mod;
         // }
   }
   v = vars; //v es la que cambia y se disminuye en la recursiva
   pol = poly;
  // powe = powers;
foo(vars, v, mod, pol, powers, sols, alfa1, alfa2, alfa3, sumato);
   // cout << mod << endl;
   // cout << " mierda " << endl;
   // casos = mod;
   // for(int i = 1; i < vars; i++){
   //    casos*=mod;
   // }
   // cout << casos << " casos" << endl;
   // cout << " pow: " << (int)pow((double)2,2) << endl;
   // casos = (pow((double)mod, (double)vars));
   // cout << casos << endl;
//for que calcula las soluciones
   // for(int l = 0; l < casos; l++){
   //    alfa1 = 0;
   //    alfa2 = 0;
   //    alfa3 = 0;
   //    //cout << "Caso: " << l+1 << " ";
   //    for (int m = 0; m < vars; m++){
   //       poly[m] = (l / base[m]) % mod;
   //       //cout << poly[m] << endl << endl << endl;
   //      //poly[m] = (l / (powers[][mod])) % mod;
   //       // cout << poly[m] << ",";
   //    }
   //    // cout << endl;
      // for (int x = 0; x < vars; x++){
      //    alfa1 += powers[0][poly[x]];
      //    alfa2 += powers[1][poly[x]];
      //    alfa3 += powers[2][poly[x]];
      //    // alfa1 += ((int)pow((double)poly[x], (double)exp_1)) % mod;
      //    // alfa2 += ((int)pow((double)poly[x], (double)exp_2)) % mod;
      //    // alfa3 += ((int)pow((double)poly[x], (double)exp_3)) % mod;
      // }
   //    alfa1 %= mod;
   //    alfa2 %= mod;
   //    alfa3 %= mod;
   //    //cout << alfa1 << " " << alfa2 << " " << alfa3 << endl;
   //    sols[alfa1][alfa2][alfa3] = sols[alfa1][alfa2][alfa3] + 1;
   //    sumato+=1;
   // }
// for que imprime las soluciones para cada posicion
   for (int i = 0; i < mod; i++){
      for (int j = 0; j < mod; j++){
         for (int k = 0; k < mod; k++){
            // cout << i << "," << j << ',' << k << ": " << sols[i][j][k] << " ";
            solus << sols[i][j][k] << " ";
         }
         // cout << endl;
      }
      solus << endl;
   }
   
   for(int i = 0; i < mod; i++){
      for(int j = 0; j < mod; j++){
         delete sols[i][j];
      }
      delete sols[i];
   }
   delete []sols;
   delete []poly;
   cout << "\n EL total de soluciones es: " << sumato << endl;
   return 0;
}

//COPYRIGHT RAMON LUIS COLLAZO MARTIS!!!
#include<iostream>
#include<cmath>
#include <fstream>

using namespace std;

typedef int* pi;
typedef pi* ppi;
typedef ppi* pppi;

//Factor Maximo Comun
int fmc(int a, int b){
   int temp; 
   while(1){
      temp = a%b;
      if(temp==0)
         return b;
      a = b;
      b = temp;
   }
}
int pow(int base, int exp, int mod){
        int p=1;
        int nbase=base;

        while(exp > 0){
                if (exp&1){
                        p= p * nbase;
                        p= p% mod;
                }
                nbase = nbase %  mod;
                nbase = nbase * nbase;
                exp = exp>>1;
        }
        return p;
}



int main(){
   int vars, exp_1, exp_2, exp_3, mod, alfa1, alfa2, alfa3;
   int *poly;
   pppi sols;
   ppi B;
   pi C;
   bool exps;
   ofstream potencias;
   potencias.open("valores.txt");
   //If file does not open, it exits the program
   if (!potencias.is_open()) {
   cout << "Unable to open file.";
	return -1; 
   }
   
   do{
      cout << "Exponente para la primera ecuacion: ";
      cin >> exp_1;
      while (exp_1 < 1){
         cout << "Exponente debe ser > 0." << endl;
         cout << "Exponente para la primera ecuacion: ";
         cin >> exp_1;
      }
      
      cout << "Exponente para la segunda ecuacion: ";
      cin >> exp_2;
      while (exp_2 < 1){
         cout << "Exponente debe ser > 0." << endl;
         cout << "Exponente para la segunda ecuacion: ";
         cin >> exp_2;
      }
   
      cout << "Exponente para la tercera ecuacion: ";
      cin >> exp_3;
      while (exp_3 < 1){
         cout << "Exponente debe ser > 0." << endl;
         cout << "Exponente para la tercera ecuacion: ";
         cin >> exp_3;
      }

      if (fmc(exp_1,exp_2) !=1 || fmc(exp_3,exp_2) !=1 || fmc(exp_1,exp_3) !=1){
         cout << "Exponentes deben ser relativamente primos." << endl;
         exps = false;
      }
      else exps = true;
   }while (!exps);

   vars = exp_1 + exp_2 + exp_3;
   poly = new int[vars];
      
   cout << "Modulo en el que se evaluara: ";
   cin >> mod;
   while (mod < 1){
      cout << "El modulo debe ser mayor que 1. Vuelva a introducirlo: ";
      cin >> mod;
   }

   sols = new ppi[mod];
   for(int i = 0; i < mod; i++){
      B = new pi[mod];
      for(int j = 0; j < mod; j++){
         C = new int[mod];
         B[j] = C;
      }
      sols[i] = B;
   }
 
   for (int i = 0; i < mod; i++)
      for (int j = 0; j < mod; j++)
         for (int k = 0; k < mod; k++)
            sols[i][j][k] = 0;

   //Los casos L 
   for(int l = 0; l < (int)(pow((double)mod, (double)vars)); l++){
      for (int m = 0; m < vars; m++){
         poly[m]=(l / (int)pow((double)mod,(double) m)) % mod;
      }
      alfa1 = 0;
      alfa2 = 0;
      alfa3 = 0;
      for (int x = 0; x < vars; x++){
         alfa1 += (pow(poly[x], exp_1, mod));
         alfa2 += (pow(poly[x], exp_2, mod));
         alfa3 += (pow(poly[x], exp_3, mod));
      }
      alfa1 %= mod;
      alfa2 %= mod;
      alfa3 %= mod;
      sols[alfa1][alfa2][alfa3]++;
   }

   for (int i = 0; i < mod; i++){
      for (int j = 0; j < mod; j++){
         for (int k = 0; k < mod; k++){
            	//if (sols[i][j][k]!=0)
		potencias << i << "," << j << ',' << k << ": " << sols[i][j][k] << endl ;
         }
         //potencias << endl;
      }
   }
   
   for(int i = 0; i < mod; i++){
      for(int j = 0; j < mod; j++){
         delete sols[i][j];
      }
      delete sols[i];
   }
   delete []sols;
   delete []poly;
   potencias.close();
   return 0;
}

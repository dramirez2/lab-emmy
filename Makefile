conform: conformula.o
	g++ -o conform conformula.o

conformula.o: conformula.cpp
	g++ -c conformula.cpp

conrec: conrecursion.o
	g++ -o conrec conrecursion.o

conrec.o: conrecursion.cpp
	g++ -c conrecursion.cpp

prime: prime.o
	g++ -o prime prime.o

prime.o: prime.cpp
	g++ -c prime.cpp

clean:
	rm *.o conform valores.txt

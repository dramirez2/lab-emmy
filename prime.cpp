#include <iostream>
#include <cmath>
using namespace std;
bool prime(int n );

int main(){
	int n;
	char op;
	do {
		cout << "Enter a number to find if it is a prime: ";
		cin >> n;
		cout << endl;
		if (prime(n)==true)
			cout << "The number inserted is a prime." << endl;
		else 
			cout << "The number inserted is compound." << endl;
		cout << "Find another prime? [y/n]: ";
		cin >> op;
		cout << endl;
	} while(op=='y' || op=='Y');
return 0;
}

bool prime(int n){
	if ((n % 2 == 0) && n != 2 && n != -2) return false;

	for (int i=3; i <= sqrt((double)n); i++)
		if (n % i == 0) return false;
	return true;
}
